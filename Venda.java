<<<<<<< HEAD
package Trabalho01Pc2.repositorio;

import Trabalho01Pc2.trabalhopc2segundoperiodo.*;
import Trabalho_01_Leonam_Teixeira_De_Vasconcelos.*;
=======
package Trabalho01Pc2.trabalhopc2segundoperiodo;

>>>>>>> As parada
import java.util.ArrayList;
import java.util.Calendar;

/**
 * @brief Classe Venda
 * @author Leonam Teixeira de Vasconcelos
 * @date   01/10/2017
 */
public class Venda {
    private static int id;
    private Calendar c =  Calendar.getInstance();
    private int cod;
    private ArrayList<Item> iList = new ArrayList<Item>();
    private String nome;
    public Venda(int dia, int mes, int ano, ArrayList<Item> iList, String nome){
        this.c.set(ano, mes, dia);
        this.iList = iList;
        this.nome = nome;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Venda.id = id;
    }

    public Calendar getC() {
        return c;
    }

    public void setC(Calendar c) {
        this.c = c;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public ArrayList<Item> getiList() {
        return iList;
    }

    public void setiList(ArrayList<Item> iList) {
        this.iList = iList;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
