<<<<<<< HEAD
package Trabalho01Pc2.repositorio;

import Trabalho01Pc2.trabalhopc2segundoperiodo.*;
=======
package Trabalho01Pc2.trabalhopc2segundoperiodo;

>>>>>>> As parada
import Trabalho_01_Leonam_Teixeira_De_Vasconcelos.*;
import java.util.ArrayList;

/**
 * @brief Classe util
 * @author Leonam Teixeira de Vasconcelos
 * @date   01/10/2017
 */
public class Util {
    public ArrayList<Produto> carregarArrayProdutos(ArrayList<Produto> pList){
        Produto a = new Produto("Arroz", "Arroz Branco tipo 1", 14.00);
        pList.add(a);
        Produto b = new Produto("Feijão", "Feijão carioca", 10.00);
        pList.add(b);
        Produto c = new Produto("Bacon", "Bacon Suíno", 8.00);
        pList.add(c);
        Produto d = new Produto("Picanha", "Picanha Bovina", 45.00);
        pList.add(d);
        Produto e = new Produto("Leite", "Leite Integral", 8.50);
        pList.add(e);
        Produto f = new Produto("Alface", "Alface Americana", 1.50);
        pList.add(f);
        Produto g = new Produto("Costela", "Costela Bovina", 25.00);
        pList.add(g);
        Produto h = new Produto("Café", "Café Torrado e Moído extra forte", 8.50);
        pList.add(h);
        Produto i = new Produto("Ovos", "Ovos de Galinha", 11.50);
        pList.add(i);
        Produto j = new Produto("Laranja", "Laranja Pêra Rio", 5.00);
        pList.add(j);
        Produto k = new Produto("Azeite", "Azeite de Oliva extra virgem", 12.00);
        pList.add(k);
        Produto l = new Produto("Macarrão", "Macarrão Espaguete", 7.00);
        pList.add(l);
        return pList;
    }
    
}
