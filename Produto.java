<<<<<<< HEAD

package Trabalho01Pc2.repositorio;

import Trabalho01Pc2.trabalhopc2segundoperiodo.*;
=======
package Trabalho01Pc2.trabalhopc2segundoperiodo;
>>>>>>> As parada

/**
 * @brief Classe Prroduto
 * @author Leonam Teixeira de Vasconcelos
 * @date   02/10/2017
 */
public class Produto {
    private static int cod_base;
    private int id;
    private String nome;
    private String desc;
    private double preco;
    
    public Produto(){
        cod_base++;
        this.id = cod_base;
    }
    
    public Produto(String nome, String desc, double preco){
        this();
        this.nome = nome;
        this.desc = desc;
        this.preco = preco;
    }

    public static int getCod_base() {
        return cod_base;
    }

    public static void setCod_base(int cod_base) {
        Produto.cod_base = cod_base;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
