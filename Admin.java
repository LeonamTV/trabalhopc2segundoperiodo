<<<<<<< HEAD
package Trabalho01Pc2.repositorio;

import Trabalho01Pc2.trabalhopc2segundoperiodo.*;
=======
package Trabalho01Pc2.trabalhopc2segundoperiodo;

>>>>>>> As parada
import Trabalho_01_Leonam_Teixeira_De_Vasconcelos.*;

/**
 * @brief Classe Admin
 * @author Leonam Teixeira de Vasconcelos
 * @date   01/10/2017
 */
public class Admin extends Pessoa{
    private String email;
    private String login;
    private String senha;
    //
    private Admin(String nome, String cpf, String email, String login, String senha){
        super(nome, cpf);
        this.email = email;
        this.login = login;
        this.senha = senha;
    }
    public Admin getInstance(String nome, String cpf, String email, String login, String senha){
        return new Admin(nome, cpf, email, login, senha);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
